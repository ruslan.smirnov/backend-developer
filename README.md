# backend developer

Развернуть на своем ПК следующий стек:  

BackEnd  
Node.js  
TypeScript  
Express.js  
База данных PostgreSQL  
Prisma ORM  

FrontEnd  
Vue.js 3  
Bootstrap 5  
Axios  

Просмотр и редактирование ресторанного меню через web-интерфейс.

Придумать форму ввода и категории позиции меню (будет плюсом, если вы придумаете какие-то интересные категории).  
Обеспечить авторизованный доступ к странице добавления позиции меню, редактированию, подгрузке изображений с ПК пользователя.  

Разработать таблицу хранения данных, разместить позиции меню и категории в таблице.  
Возможно, понадобится не одна таблица, здесь нет ограничений.  

В итоге должно получится следующее:  
веб сайт на предложенном стеке с сохранением в базе данных.  
Сайт должен содержать два раздела - один для просмотра меню, другой для редактирования.  
В первую очередь необходимо запустить сам механизм, потом работать над стилем отображения, который тоже будет учитываться.  

Можно задавать любые вопросы по заданию в телеграм @darall_support.  
После сдачи задания проведем беседу на понимание.  
Срок выполнения важен, но не лимитирован  
